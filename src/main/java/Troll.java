public class Troll {
    public static String disemvowel(String str) {
        return str
                .replaceAll("a", "")
                .replaceAll("e", "")
                .replaceAll("o", "")
                .replaceAll("i", "")
                .replaceAll("u", "")
                .replaceAll("A", "")
                .replaceAll("E", "")
                .replaceAll("O", "")
                .replaceAll("I", "")
                .replaceAll("U", "");
    }
}
